<%@ page import="model.pojo.Product" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: Grigory
  Date: 21.04.2018
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shopping list</title>
</head>
<body>
This is your shopping list

<form method="post" action="product">
    <input type="hidden" name="action" value="create">
    <input type="name" name="name">
    <input type="submit" value="create">
</form>
List of stuff you want to buy: <br>
<%
    List<Product> products = (List<Product>) request.getAttribute("products");

    for (Product product : products) {
        if (!product.isBought())
            System.out.println("<a href=\"/product?id=" +product.getId() + "\">" + product.getName() + "</a><br>");
        else
            System.out.println("<del><a href=\"/product?id=" +product.getId() + "\">" + product.getName() + "</a></del><br>");
    }
%>
</body>
</html>
