<%@ page import="model.pojo.Product" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: Grigory
  Date: 21.04.2018
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Shopping list</title>
</head>
<body>
This is your shopping list

<form method="post" action="training">
    <input type="hidden" name="action" value="create">
    <input type="date" name="date">
    <input type="time" name="time">
    <input type="submit" value="create">
</form>
Future trainigns are:
<%
    List<Product> productList = (List<Product>) request.getAttribute("products");

    for (Training training : productList) {
        out.println("<a href=\"/training?id=" +training.getId() + "\">" +
                training.getDateTime().format(DateTimeFormatter.ofPattern("HH:mm yyyy-MM-dd")) + "</a><br>");
    }
%>
</body>
</html>
