package controllers;

import model.pojo.Product;
import services.ProductService;
import services.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/list", ""})
public class ShoppingListController extends HttpServlet {
    private static final ProductService PRODUCT_SERVICE = new ProductServiceImpl();

    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        String userId = String.valueOf(servletRequest.getSession().getAttribute("userId"));
        List<Product> products = PRODUCT_SERVICE.getAllProducts(Integer.parseInt(userId));
        servletRequest.setAttribute("products", products);
        getServletContext().getRequestDispatcher("/list.jsp").forward(servletRequest, servletResponse);
    }
}
