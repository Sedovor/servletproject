package controllers;

import model.pojo.Product;
import org.apache.log4j.Logger;
import services.ProductService;
import services.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(urlPatterns = "/product")
public class ProductController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(ProductController.class);
    private static final ProductService PRODUCT_SERVICE = new ProductServiceImpl();
    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {

        int productId = Integer.parseInt(servletRequest.getParameter("id"));
        int userId = (int) servletRequest.getSession().getAttribute("userId");
        Product product = PRODUCT_SERVICE.getProduct(productId);

        if (product == null) {
            servletResponse.sendError(404);
            return;

        }

        if (product.getUserId() == userId) {
            servletRequest.setAttribute("product", product);
            getServletContext().getRequestDispatcher("/product.jsp").forward(servletRequest, servletResponse);
        } else {
            servletResponse.sendError(403);
        }
    }

    private void deleteAction(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int productId = Integer.parseInt(req.getParameter("id"));
        if (PRODUCT_SERVICE.deleteProduct(productId)) {
            resp.sendRedirect("/dashboard");
        } else {
            resp.sendError(500);
        }

    }

    private void statusAction(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {

        int productId = Integer.parseInt(servletRequest.getParameter("id"));
        boolean isBought = (boolean) servletRequest.getSession().getAttribute("isBought");
        if (PRODUCT_SERVICE.setStatus(productId, isBought)) {
            servletResponse.sendRedirect("/training?id=" + productId);
        } else {
            servletResponse.sendError(500);
        }
    }

    private void createAction(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {

        int userId = (int) servletRequest.getSession().getAttribute("userId");
        String name = String.valueOf(servletRequest.getSession().getAttribute("name"));

        Product training = new Product(-1, userId, name, false);

        if (PRODUCT_SERVICE.addProduct(training)) {
            servletResponse.sendRedirect("/dashboard");
        } else {
            servletResponse.sendError(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {

        String action = servletRequest.getParameter("action");

        switch (action) {
            case "rate":
                statusAction(servletRequest, servletResponse);
                break;
            case "delete":
                deleteAction(servletRequest, servletResponse);
                break;
            case "create":
                createAction(servletRequest, servletResponse);
                break;
            default:
                servletResponse.sendError(404);
                break;
        }

    }

}
