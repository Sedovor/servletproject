package controllers;

import model.pojo.User;
import org.apache.log4j.Logger;
import services.UserService;
import services.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/login")
public class LoginController extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(LoginController.class);
    private final UserService userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/login.jsp").forward(servletRequest, servletResponse);
    }

    @Override
    protected void doPost(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        String login = servletRequest.getParameter("login");
        String password = servletRequest.getParameter("password");
        User user = userService.getUser(login, password);

        if (user != null) {
            servletRequest.getSession().setAttribute("isAuthorised", true);
            servletRequest.getSession().setAttribute("userId", user.getId());
            servletResponse.sendRedirect("/list");
        } else {
            servletResponse.sendRedirect("/login.jsp?message=wrongPassword");
            LOGGER.debug("Wrong password for user " + login);
        }
    }
}
