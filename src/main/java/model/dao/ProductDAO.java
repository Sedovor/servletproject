package model.dao;

import model.pojo.Product;

import java.util.List;

public interface ProductDAO {
    List<Product> getAllProducts(int userId);
    boolean addProduct(Product product);
    boolean setStatus(int productId, boolean isBought);
    boolean deleteProduct(int productId);
    Product getProductById(int id);
}
