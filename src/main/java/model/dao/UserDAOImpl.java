package model.dao;

import model.DataSourceFactory;
import model.pojo.User;
import org.apache.log4j.Logger;
import java.sql.*;

public class UserDAOImpl implements UserDAO {

    private static final Logger LOGGER = org.apache.log4j.Logger.getLogger(UserDAOImpl.class);


    @Override
    public User getUserByLoginAndPassword(String login, String password) {
        User user = null;
        String sql = "SELECT * FROM users WHERE name = ? and password = ?;";

        try (Connection connection = DataSourceFactory.getDataSource().getConnection()){

            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, login);
            statement.setString(2, password);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                String userName = resultSet.getString("name");
                String userPassword = resultSet.getString("password");
                String userId = resultSet.getString("id");
                user = new User(userName, userPassword, Integer.valueOf(userId));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.debug("SQL exception while proceeding query " + sql, e);
        }

        return user;
    }
}
