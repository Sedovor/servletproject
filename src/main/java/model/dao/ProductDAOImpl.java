package model.dao;

import model.DataSourceFactory;
import model.pojo.Product;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class ProductDAOImpl implements ProductDAO {
    private static final Logger LOGGER = org.apache.log4j.Logger.getLogger(ProductDAOImpl.class);

    @Override
    public List<Product> getAllProducts(int userId) {
        List<Product> products = new ArrayList<>();

        String sql = "SELECT * FROM trainings WHERE userid = ?";

        try (Connection connection = DataSourceFactory.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, userId);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                products.add(new Product(
                        resultSet.getInt("id"),
                        resultSet.getInt("userId"),
                        resultSet.getString("name"),
                        resultSet.getBoolean("isBought")
                ));
            }

        } catch (SQLException e) {
            LOGGER.debug("SQL exception while proceeding query " + sql, e);
        }
        return products;
    }

    @Override
    public boolean addProduct(Product product) {
        boolean result = false;

        String sql = "INSERT INTO products(userid, name, status) VALUES (?, ?, ?);";

        try (Connection connection = DataSourceFactory.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, product.getUserId());
            statement.setString(2, product.getName());
            statement.setBoolean(3, product.isBought());

            if (statement.executeUpdate() == 1) {
                result = true;
            }

            result = true;

        } catch (SQLException e) {
            LOGGER.debug("SQL exception while proceeding query " + sql, e);
        }
        return result;
    }

    @Override
    public boolean setStatus(int productId, boolean isBought) {
        boolean result = false;
        String sql = "UPDATE products SET status = ? WHERE id = ?";

        try (Connection connection = DataSourceFactory.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setBoolean(1, isBought);
            statement.setInt(2, productId);
            if (statement.executeUpdate() == 1)
                result = true;
        } catch (SQLException e) {
            LOGGER.debug("SQL exception while proceeding query " + sql, e);
        }

        return result;
    }

    @Override
    public boolean deleteProduct(int productId) {
        boolean result = false;
        String sql = "DELETE FROM products WHERE id = ?";

        try (Connection connection = DataSourceFactory.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setObject(1, productId);
            if (statement.executeUpdate() == 1) {
                result = true;
            }


        } catch (SQLException e) {
            e.printStackTrace();
            LOGGER.debug("SQL exception while proceeding query " + sql, e);
        }

        return result;
    }

    @Override
    public Product getProductById(int id) {
        Product product = null;
        String sql = "SELECT * FROM products WHERE id = ?";


        try (Connection connection = DataSourceFactory.getDataSource().getConnection()) {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setObject(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                product = new Product(
                        resultSet.getInt("id"),
                        resultSet.getInt("userId"),
                        resultSet.getString("name"),
                        resultSet.getBoolean("isBought")
                );

            }

        } catch (SQLException e) {
            LOGGER.debug("SQL exception while proceeding query " + sql, e);

        }

        return product;
    }
}
