package model.dao;

import model.pojo.User;

public interface UserDAO {
    User getUserByLoginAndPassword(String login, String password);
}
