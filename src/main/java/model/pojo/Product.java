package model.pojo;

public class Product {
    private int id;
    private int userId;
    private String name;
    private boolean isBought;


    public Product(int id, int userId, String name, boolean isBought) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.isBought = isBought;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBought() {
        return isBought;
    }

    public void setBought(boolean bought) {
        isBought = bought;
    }
}
