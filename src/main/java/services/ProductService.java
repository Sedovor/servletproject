package services;

import model.pojo.Product;

import java.util.List;

public interface ProductService {
    Product getProduct(int id);
    List<Product> getAllProducts(int userId);
    boolean addProduct(Product product);
    boolean deleteProduct(int id);
    boolean setStatus(int id, boolean isBought);
}
