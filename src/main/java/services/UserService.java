package services;

import model.pojo.User;

public interface UserService {
    User getUser(String login, String password);
}
