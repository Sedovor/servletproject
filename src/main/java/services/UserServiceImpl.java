package services;

import model.dao.UserDAO;
import model.dao.UserDAOImpl;
import model.pojo.User;

public class UserServiceImpl implements UserService {
    private static final UserDAO USER_DAO = new UserDAOImpl();

    @Override
    public User getUser(String login, String password) {
        return USER_DAO.getUserByLoginAndPassword(login, password);
    }
}
