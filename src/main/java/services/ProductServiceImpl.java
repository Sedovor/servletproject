package services;

import model.dao.ProductDAO;
import model.dao.ProductDAOImpl;
import model.pojo.Product;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private static final ProductDAO PRODUCT_DAO = new ProductDAOImpl();

    @Override
    public Product getProduct(int id) {
        return PRODUCT_DAO.getProductById(id);
    }

    @Override
    public List<Product> getAllProducts(int userId) {
        return PRODUCT_DAO.getAllProducts(userId);
    }

    @Override
    public boolean addProduct(Product product) {
        return PRODUCT_DAO.addProduct(product);
    }

    @Override
    public boolean deleteProduct(int id) {
        return PRODUCT_DAO.deleteProduct(id);
    }

    @Override
    public boolean setStatus(int id, boolean isBought) {
        return PRODUCT_DAO.setStatus(id, isBought);
    }
}
